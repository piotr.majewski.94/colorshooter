package pl.pmajewski.colorshooter2.service.impl;

import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;

import pl.pmajewski.colorshooter2.memo.GameScreenMemo;
import pl.pmajewski.colorshooter2.model.BlackHoleModel;
import pl.pmajewski.colorshooter2.model.EnemyModel;
import pl.pmajewski.colorshooter2.model.EnemyModel.LaunchPosition;
import pl.pmajewski.colorshooter2.model.ShieldModel;
import pl.pmajewski.colorshooter2.model.ShieldModel.DIRECTION;
import pl.pmajewski.colorshooter2.repository.BlackHoleRepository;
import pl.pmajewski.colorshooter2.repository.EnemyRepository;
import pl.pmajewski.colorshooter2.repository.ShieldRepository;
import pl.pmajewski.colorshooter2.repository.impl.BlackHoleRepositoryImpl;
import pl.pmajewski.colorshooter2.repository.impl.EnemyRepositoryImpl;
import pl.pmajewski.colorshooter2.repository.impl.ShieldRepositoryImpl;
import pl.pmajewski.colorshooter2.service.EnemyService;
import pl.pmajewski.colorshooter2.utils.GameState;
import pl.pmajewski.colorshooter2.utils.GlobalProperties;
import pl.pmajewski.colorshooter2.utils.color.ColorManager;

public class EnemyServiceImpl implements EnemyService {
	
	// Repo
	private BlackHoleRepository bhRep = BlackHoleRepositoryImpl.getInstance();
	private ShieldRepository shieldRep = ShieldRepositoryImpl.getInstance();
	private EnemyRepository enemyRepo = EnemyRepositoryImpl.getInstance();

	private GameScreenMemo memo = GameScreenMemo.getInstance();
	
	public EnemyServiceImpl() { }

	@Override
	public void update(float delta) {
		//System.out.println("EnemyServiceImpl -> update()");
		List<EnemyModel> all = enemyRepo.listAll();
		if(System.currentTimeMillis()-memo.getLastSpeedJump() > GlobalProperties.speedJump) {
			memo.setLastSpeedJump(System.currentTimeMillis());
			memo.setSpeedScalar(memo.getSpeedScalar()*1.05f);
			//System.out.println("EnemyServiceImpl -> update() -> speedScalerUpdate: "+memo.getSpeedScalar());
		}
		//System.out.println("-------------------------------------------");
		for (Iterator iterator = all.iterator(); iterator.hasNext();) {
			EnemyModel enemy = (EnemyModel) iterator.next();
			
			
			// Usuwanie enemies ktorzy opuscili plansze
			if(enemy.getPosition().y > GlobalProperties.worldHeight + 50 || enemy.getPosition().y < -50) {
				iterator.remove();
			} else {
				boolean before = toRemove(enemy);
				Vector2 updatedPosition = enemy.getPosition().add(enemy.getVelocity());
				//System.out.println("EnemyServiceImpl -> update() -> enemy: "+enemy.getColor()+"\tvelocity: "+enemy.getVelocity());
				enemy.setPosition(updatedPosition);
				boolean after = toRemoveAfter(enemy);
				
				//System.out.println("EnemyServiceImpl -> update() -> "+enemy.hashCode()+"\tbefore: "+before+"\tafter: "+after+"\tpos: "+enemy.getPosition());
				if(before || (!before && after)) {
					//System.out.println("EnemyServiceImpl => update() -> remove enemy");
					//System.out.println("EnemyServiceImpl -> update() -> "+enemy.hashCode()+"\t[remove]");
					iterator.remove();
				}
			}
		}
	}
	
	private boolean toRemoveAfter(EnemyModel enemy) {
		//System.out.println("EnemyServiceImpl -> toRemoveAfter()");
		ShieldModel shield = shieldRep.get();
		if(shield.getPosition().dst(enemy.getPosition()) <= shield.getRadius()+enemy.getRadius() 
		&& (enemy.getVelocity().y > 0 && shield.getDirection() == DIRECTION.DOWN || enemy.getVelocity().y < 0 && shield.getDirection() == DIRECTION.TOP)) {
			return true;
		}
		return false;
	}
	
	private boolean toRemove(EnemyModel enemy) {
		ShieldModel shield = shieldRep.get();
		BlackHoleModel blackHole = bhRep.get();
		// check collision with shield
		if(shield.getPosition().dst(enemy.getPosition()) <= shield.getRadius()+enemy.getRadius()-5 && shield.getPosition().dst(enemy.getPosition()) >= shield.getRadius()+5) {
			// check if shield and enemy are on the same side
			if(enemy.getVelocity().y > 0 && shield.getDirection() == DIRECTION.DOWN || enemy.getVelocity().y < 0 && shield.getDirection() == DIRECTION.TOP) {
				return true;
			}
		} else if(blackHole.getPosition().dst(enemy.getPosition()) <= blackHole.getRadius() + enemy.getRadius()-5) {
			checkHit(enemy);
			return true;
		}
		return false;
	}
	
	private void checkHit(EnemyModel enemy) {
		BlackHoleModel blackHole = bhRep.get();
		
		if(blackHole.getColor().equals(enemy.getColor())) {
			memo.incHits();
		} else {
			double random = Math.random();
			memo.setGameState(GameState.GAME_OVER);
			memo.setGameOverHit(System.currentTimeMillis());
			memo.setGameTime(System.currentTimeMillis()-memo.getRoundStart());
			if(random < GlobalProperties.propabilityAd) {
				GlobalProperties.playService.showAd();
				memo.setAdShowed(true);
			}
		}
	}

	@Override
	public void launchEnemy(Camera camera) {
		System.out.println("EnemyServiceImpl -> launchEnemy()");

		EnemyModel enemy = new EnemyModel();
		enemy.setRadius(25);
		enemy.getShapeRenderer().setProjectionMatrix(camera.combined);
		
		Double colorPropability = Math.random();
		if(colorPropability.floatValue() <= GlobalProperties.propabilitySameColor) {
			enemy.setColor(ColorManager.getSchema().getBhColor());
		} else {
			enemy.setColor(ColorManager.getSchema().randRest());
		}
		
		boolean top;
		EnemyModel lastEnemy = enemyRepo.getLastAdded();
		if(lastEnemy != null) {
			if(lastEnemy.getLaunchPosition() == LaunchPosition.TOP) {
				top = false;
			} else {
				top = true;
			}
		} else {
			top = Math.random() > 0.5 ? true : false;
		}

		BlackHoleModel blackHole = bhRep.get();
		
		Vector2 velocity = new Vector2(1, 1);
		Vector2 position = new Vector2();
		float speed = (float) Math.random()*3+9;
		velocity = velocity.mulAdd(blackHole.getPosition(), speed);
		velocity.x *= memo.getSpeedScalar();
		velocity.y *= memo.getSpeedScalar();
		//System.out.println("EnemyServiceImpl -> launchEnemy() -> valocity: "+velocity+" speedScalar: "+memo.getSpeedScalar());
		
		position.x = (float)Math.random()*(GlobalProperties.worldWidth+GlobalProperties.worldWidth/10) - GlobalProperties.worldWidth/10;
		
		if(top) {
			position.y = GlobalProperties.worldHeight + 50;
			enemy.setLaunchPosition(LaunchPosition.TOP);
		} else {
			position.y = -50;
			enemy.setLaunchPosition(LaunchPosition.DOWN);
		}
		
		velocity.set(blackHole.getPosition().x - position.x, blackHole.getPosition().y - position.y);
		velocity.nor();
		velocity.x = velocity.x * speed*memo.getSpeedScalar();
		velocity.y = velocity.y * speed*memo.getSpeedScalar();
		
		enemy.setPosition(position);
		enemy.setVelocity(velocity);
		enemy.setSpeed(speed);
		enemy.setLaunchTime(System.currentTimeMillis());
		
//		System.out.println("EnemyServiceImpl -> launchEnemy() -> enemy.velocity: "+enemy.getVelocity());
		
		enemyRepo.add(enemy);
		memo.setLastEnemyLaunch(System.currentTimeMillis());
	}

	@Override
	public void truncate() {
		enemyRepo.truncate();
	}

	@Override
	public List<EnemyModel> listAll() {
		return enemyRepo.listAll();
	}
}
