package pl.pmajewski.colorshooter2.service;

import java.util.List;

import com.badlogic.gdx.graphics.Camera;

import pl.pmajewski.colorshooter2.model.EnemyModel;

public interface EnemyService {

	void update(float delta);

	void launchEnemy(Camera camera);

	void truncate();
	
	List<EnemyModel> listAll();
}
