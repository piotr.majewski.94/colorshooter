package pl.pmajewski.colorshooter2.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import pl.pmajewski.colorshooter2.component.AssetsManager;
import pl.pmajewski.colorshooter2.memo.GameScreenMemo;
import pl.pmajewski.colorshooter2.model.EnemyModel;
import pl.pmajewski.colorshooter2.service.BlackHoleService;
import pl.pmajewski.colorshooter2.service.EnemyService;
import pl.pmajewski.colorshooter2.service.ShieldService;
import pl.pmajewski.colorshooter2.service.impl.BlackHoleServiceImpl;
import pl.pmajewski.colorshooter2.service.impl.EnemyServiceImpl;
import pl.pmajewski.colorshooter2.service.impl.ShieldServiceImpl;
import pl.pmajewski.colorshooter2.utils.GameState;
import pl.pmajewski.colorshooter2.utils.GlobalProperties;

public class GameScreenController extends ScreenAdapter  {
	
	// Business logic services
	private BlackHoleService bhService = new BlackHoleServiceImpl();
	private ShieldService shieldService = new ShieldServiceImpl();
	private EnemyService enemyService = new EnemyServiceImpl();
	
	// Screen management
	private Batch batch;
	private OrthographicCamera camera;
	private Viewport viewport;
	
	// UI Components - images
	private AssetManager assetManager = new AssetsManager().getAssetManager();
	private TextureRegion gameOverRegion;
	private TextureRegion againRegion;
	
	// UI Components - text
	private FreeTypeFontGenerator fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("Roboto-Regular.ttf"));
	private FreeTypeFontParameter fontParameter = new FreeTypeFontParameter();
	BitmapFont counter;
	BitmapFont timer;
	
	// Memo
	private GameScreenMemo memo = GameScreenMemo.getInstance();
	
	public GameScreenController() {
		System.out.println("GameScreenController -> constructor() -> begin");
		loadAds();
		Gdx.input.setCatchBackKey(true);
		
		camera = new OrthographicCamera();
		camera.position.set(GlobalProperties.worldWidth/2, GlobalProperties.worldHeight/2, 0);
		viewport = new FitViewport(GlobalProperties.worldWidth, GlobalProperties.worldHeight, camera);
		viewport.apply();
		batch = new SpriteBatch();
		batch.setProjectionMatrix(camera.combined);
		
		fontParameter.size = 72;
		fontParameter.color = Color.WHITE;
		fontParameter.spaceX = -2;
		counter = fontGenerator.generateFont(fontParameter);
		timer = fontGenerator.generateFont(fontParameter);
		
		gameOverRegion = new TextureRegion(assetManager.get("gameover.png", Texture.class));
		againRegion = new TextureRegion(assetManager.get("AgainButton.png", Texture.class));
		
		// Fill repositiories
		bhService.generate(camera);
		shieldService.generate(camera);
		
		// Set memo
		memo.setGameState(GameState.GAME);
		memo.setHits(0);
		memo.setLastEnemyLaunch(0);
		memo.triggerRoundStart();
		
		enemyService.launchEnemy(camera);
		memo.setGameScreenLaunch(System.currentTimeMillis());
		System.out.println("GameScreenCOntroller -> constructor() -> end");
	}
	
	private void update(float delta) {
		if(memo.getGameState() == GameState.GAME) {
			//System.out.println("GameScreenController -> update() -> System.currentTimeMillis() - memo.getLastEnemyLaunch():"+(System.currentTimeMillis() - memo.getLastEnemyLaunch()));
			if(System.currentTimeMillis() - memo.getLastEnemyLaunch() >= 600) {
				enemyService.launchEnemy(camera);
			}
			
			if(Gdx.input.justTouched()) {
				shieldService.rotate();
			}
			
			enemyService.update(delta);
		}
		
		if(memo.getGameState() == GameState.GAME_OVER) {
			if(Gdx.input.justTouched() && System.currentTimeMillis() - memo.getGameOverHit() > GlobalProperties.gameOverPause) {
				restartGame();			
			}
		}
		
		if(Gdx.input.isKeyPressed(Keys.BACK) && (System.currentTimeMillis() - memo.getLastBackButtonPressed() > 250)) {
			if(memo.getGameState() == GameState.GAME) {
				memo.setGameState(GameState.GAME_OVER);
			} else {
				restartGame();
			}
			
			memo.setLastBackButtonPressed(System.currentTimeMillis());
			//GlobalProperties.gameInstace.setScreen(new MainMenuController());
			//dispose();
		}
	}
	
	private int fpsCounter = 0;
	private long lastFPSDisplay = 0;
	@Override
	public void render(float delta) {
		if(System.currentTimeMillis() - lastFPSDisplay > 1000) {
			lastFPSDisplay = System.currentTimeMillis();
			System.out.println("fps: "+fpsCounter);
			fpsCounter = 0;
		} else {
			fpsCounter++;
		}
		
		
		long begin = System.currentTimeMillis();
		//System.out.println("GameScreenController -> render()");
		//System.out.println("  |--gameState: "+memo.getGameState());
		update(delta);
		
		Gdx.gl.glClearColor(1f/255f*33f, 1f/255f*33f, 1f/255f*33f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glEnable(GL20.GL_SAMPLES);
		camera.update();
		
		shieldService.get().draw();
		for (EnemyModel temp : enemyService.listAll()) {
			temp.draw();
		}
		bhService.get().draw();
		
		batch.begin();
		batch.setProjectionMatrix(camera.combined);
		
		counter.draw(batch, Integer.toString(memo.getHits()), 15, GlobalProperties.worldHeight-15);
		timer.draw(batch, getRoundTime(), GlobalProperties.worldWidth-150, GlobalProperties.worldHeight-15);
		
		if(memo.getGameState() == GameState.GAME_OVER) {
			batch.draw(gameOverRegion, GlobalProperties.worldWidth/2-(gameOverRegion.getRegionWidth()/2), GlobalProperties.worldHeight/4*3 - (gameOverRegion.getRegionHeight()/2));
			batch.draw(againRegion, GlobalProperties.worldWidth/2-384/2, GlobalProperties.worldHeight/2-384/2);
		}
		batch.end();
		//System.out.println("TimePerFrame: "+(System.currentTimeMillis()-begin)+" ms");
	}
	
	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
		camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
		batch.setProjectionMatrix(camera.combined);
	}
	
	@Override
	public void dispose() {
		super.dispose();
		bhService.truncate();
		shieldService.truncate();
		enemyService.truncate();
		memo.truncate();
		
		memo.setGameState(GameState.GAME);
		memo.setHits(0);
		memo.setLastEnemyLaunch(0);
		memo.triggerRoundStart();
		memo.setGameScreenLaunch(System.currentTimeMillis());
		memo.setSpeedScalar(1);
		
		assetManager.dispose();
	}
	
	public void restartGame() {
		loadAds();
		bhService.truncate();
		shieldService.truncate();
		enemyService.truncate();
		memo.truncate();
		
		bhService.generate(camera);
		shieldService.generate(camera);
		//enemyService.launchEnemy(camera);
		
		// Set memo
		memo.setGameState(GameState.GAME);
		memo.setHits(0);
		memo.setLastEnemyLaunch(0);
		memo.triggerRoundStart();
		memo.setGameScreenLaunch(System.currentTimeMillis());
		memo.setSpeedScalar(1);
		memo.setRoundStart(System.currentTimeMillis());
	}
	
	private void loadAds() {
		if(memo.isAdShowed()) {
			GlobalProperties.playService.loadAd();
			memo.setAdShowed(false);
		}
	}
	
	private String getRoundTime() {
		long roundStart = memo.getRoundStart();
		long roundTime = System.currentTimeMillis() - roundStart;
		if(memo.getGameState() == GameState.GAME_OVER) {
			roundTime = memo.getGameTime();
		}
		
		StringBuilder sb  = new StringBuilder();
		if(roundTime < 1000) {
			sb.append("0:");
			sb.append(roundTime/10);
		} else if(roundTime < 1000 * 10)  {
			sb.append(roundTime/1000);
			sb.append(":");
			sb.append(roundTime%1000/10);			
		} else if(roundTime < 1000 * 60)  {
			sb.append(roundTime/1000);
			sb.append(":");
			sb.append(roundTime%1000/100);
		} else if(roundTime < (1000*60) + (1000*10)) {
			sb.append(roundTime/1000/60);
			sb.append(":0");
			sb.append(roundTime/1000%60);
		} else {
			sb.append(roundTime/1000/60);
			sb.append(":");
			sb.append(roundTime/1000%60);
		}
		return sb.toString();
	}
}
