package pl.pmajewski.colorshooter2.utils.color;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;

public class ColorSchema {
	
	private Color bgColor;
	private Color bhColor;
	private Color shieldColor;
	private List<Color> rest = new ArrayList<Color>();
	
	public ColorSchema() { }
	
	public ColorSchema(Color bgColor, Color bhColor, Color shieldColor, List<Color> rest) {
		super();
		this.bgColor = bgColor;
		this.bhColor = bhColor;
		this.shieldColor = shieldColor;
		this.rest = rest;
	}

	public Color getBgColor() {
		return bgColor;
	}

	public void setBgColor(Color bgColor) {
		this.bgColor = bgColor;
	}

	public Color getBhColor() {
		return bhColor;
	}

	public void setBhColor(Color bhColor) {
		this.bhColor = bhColor;
	}

	public Color getShieldColor() {
		return shieldColor;
	}

	public void setShieldColor(Color shieldColor) {
		this.shieldColor = shieldColor;
	}

	public List<Color> getRest() {
		return rest;
	}

	public void setRest(List<Color> rest) {
		this.rest = rest;
	}
	
	public void addRestColor(Color color) {
		rest.add(color);
	}
	
	public Color randRest() {
		Double rand = Math.random()*rest.size();
		return rest.get(rand.intValue());
	}
}
