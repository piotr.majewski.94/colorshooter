package pl.pmajewski.colorshooter2.utils;

public class StateManager {
	
	public static enum State {
		GAME, GAME_OVER, TEST;
	}
	
	private State state;

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
}
