package pl.pmajewski.colorshooter2.repository;

import pl.pmajewski.colorshooter2.model.ShieldModel;

public interface ShieldRepository {

	ShieldModel get();

	void truncate();
	
	void add(ShieldModel shield);
}
